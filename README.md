# Battery Applications 🔋⚡

This repository hosts solutions to the programming assignments accompanying the lecture _Battery Applications_.

The solutions are presented as Python-based Jupyter notebooks, serving as illustrative suggestions to convey the concepts. We strongly encourage implementing your own solutions, whether in Python or any preferred programming language or tool such as Julia, MATLAB, or Excel

The notebooks can be easily executed online via Binder, providing a readily available environment without the necessity of a Python installation. Alternatively, you can run them on your local machine by following the setup instructions provided below.

## Set-up

### 1. Create a virtual environment
Create and activate a virtual environment, for example with either
[venv](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment) or [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html), or directly through your IDE. [Visual Studio Code](https://code.visualstudio.com/docs/python/environments) is recommended.

### 2. Install dependencies
Install all other required Python packages in your virtual environment. This can be done with a single command with the provided "requirements" file:
```
pip install -r requirements.txt
```
Some IDEs, like [VS Code](https://code.visualstudio.com/docs/python/environments) 
may install the requirements automatically when setting up the virtual environment.

### 3. Launch Jupyter
Start Jupyter with the following command:
```
jupyter lab
```

Alternatively, for an integrated and recommended experience, [VS Code](https://code.visualstudio.com/docs/datascience/jupyter-notebooks) provides a direct working environment for Jupyter notebooks.


## License
This repository is licensed under MIT License.